/* eslint-disable no-unused-vars */
import { useDispatch, useSelector } from "react-redux"
import { fetchPosts } from "./store/actions/posts"
import { useEffect, useRef, useState } from "react"
import { useMemo } from "react"
import MyForm from "./components/MyForm"
import FormikForm from "./components/FormikForm"
import ProductList from "./components/ProductList/ProductList"
import ProductListFN from "./components/ProductList/ProductListFn"
import PerformanceTask from "./components/PerformanceTask/Main"

function App() {
  const posts = useSelector(state => state.posts.data)
  const isLoading = useSelector(state => state.posts.loading)
  const error = useSelector(state => state.posts.error)

  // const postValue = useSelector(state => state.posts.value)
  // const postError = useSelector(state => state.posts.error)
  const dispatch = useDispatch()

  useEffect(() => {
    // dispatch(fetchPosts())
  }, [])

  return (
    <>
      {/* <PerformanceTask /> */}
      <ProductListFN />
      {/* <FormikForm /> */}

      {/* <MyForm />

      {!!error && <div>Error happened</div>}
      {!!isLoading && <div>Loading...</div>}
      {posts && Boolean(posts.length) && posts.map(post =>
        <div key={post.id}>
          <h2>{post.title}</h2>
          <p>{post.body}</p>
        </div>
      )} */}
    </>
  )
}

export default App