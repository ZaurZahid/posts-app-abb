/* eslint-disable no-undef */
import ProductListFN from "../components/ProductList/ProductListFn"
import { render, screen } from "@testing-library/react"

test("should render product list component", () => {
    render(<ProductListFN />)
    const favoriteListContainer = screen.getByTestId('favorite-list')
    expect(favoriteListContainer).toBeInTheDocument()
})