/* eslint-disable no-unused-vars */
import { ErrorMessage, Field, Form, Formik } from 'formik';
import { userSchema } from '../validation/user';

const FormikForm = () => {
    console.log(userSchema)
    return (
        <div>
            <h1>Anywhere in your app!</h1>
            <Formik
                validationSchema={userSchema}
                initialValues={{ login: '', password: '', confirmPassword: '' }}
                // validate={values => {
                //     const errors = {};

                //     if (!values.email) {
                //         errors.email = 'Required';
                //     } else if (
                //         !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                //     ) {
                //         errors.email = 'Invalid email address';
                //     }
                //     return errors;
                // }}
                onSubmit={(values, { setSubmitting }) => {
                    setTimeout(() => {
                        alert(JSON.stringify(values, null, 2));
                        setSubmitting(false);
                    }, 400);
                }}
            >
                {({
                    isSubmitting,
                    errors
                    /* and other goodies */
                }) => {
                    console.log(errors)
                    return (
                        <Form>
                            <Field type="text" name="login" />
                            <ErrorMessage name="login" component="div" />
                            <Field type="text" name="password" />
                            <ErrorMessage name="password" component="div" />

                            <Field type="text" name="confirmPassword" />
                            <ErrorMessage name="confirmPassword" component="div" />

                            {/* <Field type="email" name="email" />
                        <ErrorMessage name="email" component="div" />
                        <Field type="password" name="password" />
                        <ErrorMessage name="password" component="div" /> */}
                            <button type="submit" disabled={isSubmitting}>
                                Submit
                            </button>
                        </Form>
                    )
                }}
            </Formik>
        </div>
    )
}

export default FormikForm;