import { useEffect, useRef, useState } from "react"
import { useMemo } from "react"

function MyForm() {
    const [userName, setUserName] = useState('')
    const [userAge, setUserAge] = useState('')
    const [errors, setErrors] = useState({})
    const [touched, setTouched] = useState({})

    const userNameRef = useRef(null)
    const userImageRef = useRef(null)
    // console.log(userNameRef.current)

    useEffect(() => {
        if (userNameRef && userNameRef.current) {
            userNameRef.current.value = "Zaur"
            userNameRef.current.focus()
        }
    }, [])

    console.log(errors)

    const handleBlur = (e) => {
        const { name } = e.target;
        setTouched({ ...touched, [name]: true });
    }

    console.log(touched)


    const validateForm = () => {
        const validationErrors = {};
        if (!userName.length) {
            validationErrors.userName = 'This field is required';
        }

        if (!userAge) {
            validationErrors.userAge = 'This field is required';
        }


        setErrors(validationErrors);
        if (Object.values(validationErrors).filter(n => n).length) return true
    }

    useEffect(() => {
        validateForm()
    }, [])

    function onSubmit(e) {
        e.preventDefault()

        const isInValid = validateForm()
        console.log(isInValid)
        if (isInValid) alert('fix issues')
        alert(`${userName}, ${userAge}`)
    }

    console.log(errors)
    console.log(Object.values(errors))

    const isDisabled = useMemo(() => Object.values(errors).filter(n => n).length, [errors])
    console.log(isDisabled)

    return (
        <form onSubmit={onSubmit}>
            <label htmlFor="user-name">Controlled Input</label>
            <input
                type="text"
                value={userName}
                onChange={(e) => {
                    setErrors(prev => {
                        return {
                            ...prev,
                            userName: ""
                        }
                    })
                    setUserName(e.target.value)
                }}
                id="user-name"
                name="userName"
                onBlur={handleBlur}
            />

            {touched.userName && errors?.userName ? <span style={{ color: "red" }}>{errors?.userName}</span> : null}

            {/* <label htmlFor="user-name-2">UnControlled Input</label>
      <input
        type="text"
        id="user-name-2"
        name="user-name-2"
        ref={userNameRef}
      /> */}

            <label htmlFor="user-age">User age</label>
            <input
                type="text"
                id="user-age"
                value={userAge}
                onChange={(e) => {
                    setErrors(prev => {
                        return {
                            ...prev,
                            userAge: ""
                        }
                    })
                    setUserAge(e.target.value)
                }}
                name="userAge"
                onBlur={handleBlur}
            />
            {touched.userAge && errors?.userAge ? <span style={{ color: "red" }}>{errors?.userAge}</span> : null}
            <button type="submit" disabled={isDisabled}>Submit</button>


            <label htmlFor="user-image">User image</label>
            <input
                style={{ display: 'none' }}
                type="file"
                id="user-image"
                name="user-image"
                ref={userImageRef}
            />

            <div onClick={() => userImageRef.current?.click()} style={{ height: 100, width: 200, border: "1px dashed black" }}>Drop your image here</div>
        </form>
    )
}

export default MyForm