import React, { useState } from 'react';

function DebounceExample() {
    const [inputValue, setInputValue] = useState('');
    const [debouncedValue, setDebouncedValue] = useState('');

    // Debounce function
    const debounce = (func, delay) => {
        let timerId;
        return (...args) => {
            clearTimeout(timerId);
            timerId = setTimeout(() => {
                func(...args);
            }, delay);
        };
    };

    const handleInputChange = (event) => {
        const newValue = event.target.value;
        setInputValue(newValue);

        // Debounce the update of debouncedValue
        const delayedUpdate = debounce((value) => {
            setDebouncedValue(value);
        }, 1000); // Adjust the delay (in milliseconds) as needed

        delayedUpdate(newValue);
    };


    return (
        <div>
            <input
                type="text"
                value={inputValue}
                onChange={handleInputChange}
                placeholder="Type something..."
            />
            <p>Input value: {inputValue}</p>
            <p>Debounced value: {debouncedValue}</p>
        </div>
    );
}

export default DebounceExample;
