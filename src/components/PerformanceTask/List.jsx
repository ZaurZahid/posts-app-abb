/* eslint-disable react/prop-types */
/* eslint-disable react-refresh/only-export-components */
import { memo } from 'react';
import ListItem from './ListItem';

const List = ({ users, onRemove }) => {
    console.log('User list is running')

    return (
        <ul>
            {users.map(user => {
                return (
                    <ListItem key={user.id} onRemove={onRemove} user={user} />
                )
            })}
        </ul>
    )
}

export default memo(List)