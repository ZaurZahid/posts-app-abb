import { memo, useContext } from "react"
import { ThemeContext } from "./Main"

/* eslint-disable react/prop-types */
function ListItem({ user, onRemove }) {
    const theme = useContext(ThemeContext)
    console.log('ListItem is running')

    return (
        <div style={{ display: "flex", background: theme === 'dark' ? "black" : "white", color: theme === 'dark' ? "white" : "black" }}>
            <h4>Name: {user.name}</h4>
            <button onClick={() => onRemove(user.id)}>X</button>
        </div>
    )
}

export default memo(ListItem)