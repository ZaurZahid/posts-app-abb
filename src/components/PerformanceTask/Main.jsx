/* eslint-disable react/prop-types */
import { createContext, useCallback, useMemo, useState } from "react"
import List from "./List"
import useSearch from "./useSearch"

export const ThemeContext = createContext(null)

function PerformanceTask() {
    const [theme, setTheme] = useState('light')
    console.log('App is running')
    const { filterValue, searchText, handleChangeFilterValue } = useSearch()

    const [users, setUsers] = useState([
        { id: 1, name: "Mark" },
        { id: 2, name: "Zack" },
    ])

    const [text, setText] = useState('')

    const handleChangeValue = (e) => {
        console.log('handleChangeValue', e.keyCode)
        setText(e.target.value)
    }

    const addNewUser = () => {
        setUsers(prev => [...prev, { id: Math.random() * 100, name: text }])
        setText('')
    }

    const removeUser = useCallback((id) => {
        setUsers(users.filter(item => item.id !== id))
        setText('')
    }, [users])

    const filteredUsers = useMemo(() => users.filter(user => user.name.toLowerCase().includes(filterValue)), [filterValue, users])

    const userList = useMemo(() => (
        <ul>
            {users.map(user => {
                return (
                    <li key={user.id}>
                        Name: {user.name}
                    </li>
                )
            })}
        </ul>
    ), [users])

    return (
        <ThemeContext.Provider value={theme}>
            <div>
                <button onClick={() => setTheme(prev => prev === 'light' ? "dark" : 'light')}>Switch</button>
                {/* <DebounceExample /> */}
                <input type="text" onChange={handleChangeFilterValue} placeholder="Search user" value={searchText} />
                <br />

                <input type="text" onChange={handleChangeValue} />
                <button onClick={addNewUser}>Add User</button>
                {userList}
                {/* <List users={filteredUsers} onRemove={removeUser} /> */}
            </div>
        </ThemeContext.Provider>
    )
}

export default PerformanceTask