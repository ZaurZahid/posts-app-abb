import debounce from "../../utils"
import { useCallback, useState } from "react"

function useSearch() {
    const [filterValue, setFilterValue] = useState('')
    const [searchText, setSearchText] = useState('')

    const runDebouncing = useCallback(() => {
        const delayedUpdate = debounce((value) => {
            setFilterValue(value)
        }, 2000)

        delayedUpdate(searchText)
    }, [searchText])

    const handleChangeFilterValue = (e) => {
        setSearchText(e.target.value)

        runDebouncing()
    }

    return { filterValue: filterValue.toLowerCase(), searchText, handleChangeFilterValue }
}

export default useSearch