import classNames from 'classnames'
import styles from './Button.module.css'

function Button({ type }) {
    return (
        <div className={
            classNames(
                styles['button'],
                { [styles['button-warning']]: type === 'warning' },
                { [styles['button-success']]: type === 'success' },
                { [styles['button-error']]: type === 'error' },
            )
        }>Button</div>
    )
}

export default Button