/* eslint-disable react/prop-types */
import { Component } from 'react'
import styles from './ProductItem.module.css'

export default class ProductItem extends Component {
    constructor(props) {
        super(props)
    }

    shouldComponentUpdate(nextProps) {
        // console.log(nextProps.product)
        if (nextProps.product.isFav !== this.props.product.isFav || nextProps.product.isInBasket !== this.props.product.isInBasket) {
            return true
        }

        return false
    }

    componentDidUpdate() {
        // console.log('componentDidUpdate is running')
        // console.log('-------------------------------------')
    }

    componentDidCatch() {
        console.log('componentDidCatch is running')
        console.log('-------------------------------------')
    }

    componentWillUnmount() {
        console.log('componentWillUnmount is running')
        console.log('-------------------------------------')
    }

    render() {
        const { onFavItem, addToBasket, product } = this.props
        const { name, author } = product

        // console.log(product)

        const handleFav = (product) => {
            const clonedProduct = { ...product }

            clonedProduct.isFav = !clonedProduct.isFav;
            onFavItem(clonedProduct)
        }

        const handleBasket = (product) => {
            const clonedProduct = { ...product }

            clonedProduct.isInBasket = !clonedProduct.isInBasket;
            addToBasket(clonedProduct)
        }

        return (
            <div className={styles['container']}>
                <div className={styles['product-item']}>
                    <p> Name: {name}</p>
                    <p> Author: {author}</p>
                    <button className='' onClick={() => handleFav(product)}>{product?.isFav ? "Added" : "Add to Favorite"}</button>
                    <button className='' onClick={() => handleBasket(product)}>{product?.isInBasket ? "Added" : "Add to Basket"}</button>
                </div>
            </div>
        )
    }
}
