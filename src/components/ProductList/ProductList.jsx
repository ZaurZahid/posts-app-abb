import { Component } from 'react'
import ProductItem from './ProductItem'
import styles from './ProductList.module.css'

const productList = [
    {
        id: 1,
        name: "0 to 1",
        author: "Peter Thiel"
    },
    {
        id: 2,
        name: "The Lean Startup",
        author: "Eric Ries"
    },
    {
        id: 3,
        name: "Atomic Habits",
        author: "James Clear"
    },
    {
        id: 4,
        name: "Sapiens: A Brief History of Humankind",
        author: "Yuval Noah Harari"
    },
    {
        id: 5,
        name: "Thinking, Fast and Slow",
        author: "Daniel Kahneman"
    },
    {
        id: 6,
        name: "The Power of Habit",
        author: "Charles Duhigg"
    },
    {
        id: 7,
        name: "Good to Great",
        author: "Jim Collins"
    },
    {
        id: 8,
        name: "The Art of War",
        author: "Sun Tzu"
    },
    {
        id: 9,
        name: "Start with Why",
        author: "Simon Sinek"
    },
    {
        id: 10,
        name: "Rich Dad Poor Dad",
        author: "Robert Kiyosaki"
    }
];


class ProductList extends Component {
    constructor() {
        super();

        this.state = {
            loading: true,
            error: false,
            products: [],
            favList: [],
            basketList: [],
        }
    }

    componentDidMount() {
        console.log('componentDidMount works')
        console.log('---------------------------')

        setTimeout(() => {
            this.setState(prev => {
                return {
                    ...prev,
                    loading: false,
                    error: false,
                    products: productList,
                    favList: JSON.parse(localStorage?.getItem('favList')) || []
                }
            })
        }, 500);
    }

    componentDidUpdate(_, prevState) {
        if (prevState.favList.length !== this.state.favList.length) {
            localStorage.setItem('favList', JSON.stringify(this.state.favList))
        }
    }

    onFavItem(product) {
        this.setState(prev => {
            let newProductList = [...this.state.products]
            newProductList = newProductList.map(item => item.id === product.id ? product : item)

            let newFavList
            const isProductInFavList = this.state.favList.some(item => item.id === product.id);

            if (isProductInFavList) {
                newFavList = this.state.favList.filter(item => item.id !== product.id);
            } else {
                newFavList = [...this.state.favList, product]
            }

            return {
                ...prev,
                products: newProductList,
                favList: newFavList
            }
        })
    }

    addToBasket(product) {
        this.setState(prev => {
            let newProductList = [...this.state.products]
            newProductList = newProductList.map(item => item.id === product.id ? product : item)

            return {
                ...prev,
                products: newProductList
            }
        })
    }

    render() {
        if (this.state.loading) return <div>Loading</div>
        if (this.state.error) return <div>Error happened</div>

        return (
            <div className={styles['top-container']}>
                <div className={styles['container']}>
                    <h1>Favorite list</h1>
                    {this.state.favList.map((product) => {
                        return (
                            <ProductItem
                                key={product.id}
                                product={product}
                                onFavItem={(product) => this.onFavItem(product)}
                                addToBasket={(product) => this.addToBasket(product)}
                            />
                        )
                    })}
                </div>

                <div className={styles['container']}>
                    <h1>Product list</h1>
                    {this.state.products.map((product) => {
                        return (
                            <ProductItem
                                key={product.id}
                                product={product}
                                onFavItem={(product) => this.onFavItem(product)}
                                addToBasket={(product) => this.addToBasket(product)}
                            />
                        )
                    })}
                </div>


            </div>
        )
    }
}

export default ProductList