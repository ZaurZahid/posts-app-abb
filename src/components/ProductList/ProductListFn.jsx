import { useEffect, useState } from 'react'
import ProductItem from './ProductItem'
import styles from './ProductList.module.css'
import styles2 from './ProductItem.module.css'
import classNames from 'classnames';
import Button from './Button';
import styled, { css } from 'styled-components'

const productList = [
    {
        id: 1,
        name: "0 to 1",
        author: "Peter Thiel"
    },
    {
        id: 2,
        name: "The Lean Startup",
        author: "Eric Ries"
    },
    {
        id: 3,
        name: "Atomic Habits",
        author: "James Clear"
    },
    {
        id: 4,
        name: "Sapiens: A Brief History of Humankind",
        author: "Yuval Noah Harari"
    },
    {
        id: 5,
        name: "Thinking, Fast and Slow",
        author: "Daniel Kahneman"
    },
    {
        id: 6,
        name: "The Power of Habit",
        author: "Charles Duhigg"
    },
    {
        id: 7,
        name: "Good to Great",
        author: "Jim Collins"
    },
    {
        id: 8,
        name: "The Art of War",
        author: "Sun Tzu"
    },
    {
        id: 9,
        name: "Start with Why",
        author: "Simon Sinek"
    },
    {
        id: 10,
        name: "Rich Dad Poor Dad",
        author: "Robert Kiyosaki"
    }
];

function ProductListFN() {
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);
    const [products, setProducts] = useState();
    const [favList, setFavList] = useState(() => {
        const localFavList = JSON.parse(localStorage?.getItem('favList'));
        return localFavList || [];
    });

    useEffect(() => {
        console.log('componentDidMount works');
        console.log('---------------------------');

        setTimeout(() => {
            setLoading(false);
            setError(false);
            setProducts(productList);
        }, 500);
    }, []);

    // useEffect(() => {
    //     const localData = JSON.parse(localStorage?.getItem('favList'))
    //     if (favList.length !== localData.length) {
    //         localStorage.setItem('favList', JSON.stringify(favList));
    //     }
    // }, [favList]);

    const onFavItem = (product) => {
        const newProductList = products.map(item => item.id === product.id ? product : item);

        const isProductInFavList = favList.some(item => item.id === product.id);
        let newFavList = [];

        if (isProductInFavList) {
            newFavList = favList.filter(item => item.id !== product.id);
        } else {
            newFavList = [...favList, product];
        }

        console.log(newFavList)

        setProducts(newProductList);
        setFavList(newFavList);
    };

    const addToBasket = (product) => {
        const newProductList = products.map(item => item.id === product.id ? product : item);
        setProducts(newProductList);
    };

    if (loading) return <div>Loading</div>;
    if (error) return <div>Error happened</div>;
    console.log(typeof styled.h1)
    return (
        <div className={[styles['top-container'], styles['top-container2']].join(' ')} data-testid='favorite-list'>
            <DashBoardContainer>
                <DashboardHeader>DashboasdsdsrdHeader</DashboardHeader>
                <DashboardHeader2>DashboardHeader2</DashboardHeader2>
                <DashboardContent background="red">DashboardContent</DashboardContent>
            </DashBoardContainer>


            {/* <Button />
            <Button type="warning" />
            <Button type="success" />
            <Button type="error" />
            <div className={styles['container']}>
                <h1>Favorite list</h1>
                {favList.map((product) => (
                    <ProductItem
                        key={product.id}
                        product={product}
                        onFavItem={(product) => onFavItem(product)}
                        addToBasket={(product) => addToBasket(product)}
                    />
                ))}
            </div>

            <div className={styles2['container']}>
                <h1>Product list</h1>
                {products.map((product) => (
                    <ProductItem
                        key={product.id}
                        product={product}
                        onFavItem={(product) => onFavItem(product)}
                        addToBasket={(product) => addToBasket(product)}
                    />
                ))}
            </div> */}
        </div>
    );
}

const DashBoardContainer = styled.div`
   display:block;
   background:white
`

const DashboardHeader = styled.h1`
font-size:28px;
color:purple
`

const DashboardHeader2 = styled(DashboardHeader)`
    color:orange
`

const DashboardContent = styled.section`
color:blue;

${props => css`
    background: ${props.background};
    color: white;
  `}
`

export default ProductListFN;