export const ActionTypes = {
  INCREMENT: "INCREMENT",
  DECREMENT: "DECREMENT",
  REACHED: "REACHED",
  LOADING_POSTS: 'LOADING_POSTS',
  SUCCESS_POSTS: 'SUCCESS_POSTS',
  ERROR_POSTS: 'ERROR_POSTS',
}

// Action creators
export const loadingPosts = () => {
  return {
    type: ActionTypes.LOADING_POSTS,
  };
};

export const fetchPosts = () => {
  return async (dispatch) => {
    dispatch({
      type: ActionTypes.LOADING_POSTS,
    });

    try {
      const response = await fetch('https://jsonplaceholder.typicode.com/posts')
      const data = await response.json()
      dispatch({
        type: ActionTypes.SUCCESS_POSTS,
        payload: data.slice(0, 5)
      });
    } catch (error) {
      dispatch({
        type: ActionTypes.ERROR_POSTS,
      });
    }
  }

  // new Promise((resolve, reject) => {
  //   dispatch({
  //     type: ActionTypes.LOADING_POSTS,
  //   });

  //   try {
  //     const response = fetch('https://jsonplaceholder.typicode.com/posts')
  //     response.then(res => res.json()).then(data => {
  //       dispatch({
  //         type: ActionTypes.SUCCESS_POSTS,
  //         payload: data.slice(0, 5)
  //       });
  //     })
  //     resolve(response)
  //   } catch (error) {
  //     reject(error)
  //     dispatch({
  //       type: ActionTypes.ERROR_POSTS,
  //     });
  //   }
  // })
  // }
}


export const errorPosts = () => {
  return {
    type: ActionTypes.ERROR_POSTS,
  };
};


export const increment = () => {
  return {
    type: ActionTypes.INCREMENT,
  };
};

export const decrement = () => {
  return {
    type: ActionTypes.DECREMENT,
  };
};

export const reached = () => {
  return {
    type: ActionTypes.REACHED,
  };
};
