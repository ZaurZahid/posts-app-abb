import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './reducers';
// import { decrement, increment } from './actions/posts';
import thunkMiddleware from "redux-thunk";

// const firstMiddleware = (store) => (next) => (action) => next(action)
// const secondMiddleware = (store) => (next) => (action) => next(action)
// const thirdMiddleware = (store) => (next) => (action) => {
//     const postValue = store.getState().posts.value
//     console.log('postValue is: ', postValue)

//     if (postValue >= 10) {
//         return next({
//             type: "reached"
//         })
//     }

//     if (action.type === 'DECREMENT') {
//         next(decrement())
//     }

//     if (action.type === 'INCREMENT') {
//         next(increment())
//     }
// }
const env = 'developement'
const composeEnhancers =
    env === 'developement'
        ? (window).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
        : compose;

const store = createStore(
    rootReducer,
    composeEnhancers(
        applyMiddleware(
            thunkMiddleware
            // firstMiddleware,
            // secondMiddleware,
            // thirdMiddleware
        )
    )
);


export default store;