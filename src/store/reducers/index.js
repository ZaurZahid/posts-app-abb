import { combineReducers } from "redux";
import { postsReducer } from "./posts";

const rootReducer = combineReducers({
    // users: postsReducer,
    posts: postsReducer,
    // comments: postsReducer
})

export default rootReducer