
const initialstate = {
    data: [],
    error: null,
    loading: false,
    value: 0
}

export const postsReducer = (state = initialstate, action) => {
    switch (action.type) {
        case 'LOADING_POSTS':
            return {
                ...state,
                error: false,
                loading: true
            }
        case 'SUCCESS_POSTS':
            return {
                ...state,
                data: action.payload,
                error: false,
                loading: false
            }
        case 'ERROR_POSTS':
            return {
                ...state,
                data: [],
                error: true,
                loading: false
            }

        /* ----------------------------------------------------- */
        case 'INCREMENT':
            return {
                ...state,
                value: state.value + 1
            }
        case 'DECREMENT':
            return {
                ...state,
                value: state.value - 1
            }

        case 'reached':
            return {
                ...state,
                value: 0,
                error: "U have reached"
            }

        default:
            return state
    }
}