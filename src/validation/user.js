import * as yup from 'yup'

export const userSchema = yup.object().shape({
    login: yup
        .string()
        .required('This field is required')
        .email('Please enter a valid email'),
    password: yup
        .string()
        .required('This field is required')
        .min(8, 'Minimum password length is 8 symbols')
        .max(9, 'Maximum password length is 9 symbols'),
    confirmPassword: yup
        .string()
        .required('This field is required')
        .oneOf([yup.ref('password')], 'Passwords do not match')
});